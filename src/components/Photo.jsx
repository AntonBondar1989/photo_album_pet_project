import React from "react";




export function Photo({ author, url }) {

  return (
    <div className="photo">
      <img className="phot_size" src={url} alt="Item" />
      <h4>{author}</h4>
    </div>

  );
}

