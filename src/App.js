import React, { useEffect, useState } from 'react';
import './App.css';
import Main from './Main';




function App() {
  // Хуки
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [searchValue, setSearchValue] = useState('');
  const [photo, setPhoto] = useState([]);
  const [numberOfPhotos, setNumberOfPhotos] = useState(10);

  // Запрос на сервак
  useEffect(() => {
    fetch(`https://picsum.photos/v2/list?page=${page}&limit=${numberOfPhotos}`).
      then(res => res.json()).
      then(json => {
        setPhoto(json)
      })
      .catch(err => {
        console.warn(err);
        alert("Error while getting data");
      })
      .finally(() => setLoading(false))
  }, [page])

  return (
    <div className="App">
      <Main
        loading={loading}
        setLoading={setLoading}
        page={page}
        setPage={setPage}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        photo={photo}
        setPhoto={setPhoto}
        numberOfPhotos={numberOfPhotos}
        setNumberOfPhotos={setNumberOfPhotos}
      />
    </div >
  );
}

export default App;
