import './App.css';
import { Photo } from './components/Photo';


function Main(props) {
   return (
      <div className="App">
         <h1>All Photos</h1>
         <ul className="pagination">
            {
               // Пагинация
               [...Array(5)].map((_, i) => (
                  <li key={i} onClick={() => props.setPage(i + 1)} className={props.page === i + 1 ? 'active' : ''}>
                     {i + 1}
                  </li>
               )
               )
            }
         </ul>
         <div className="top">
            {/* Поиск автора */}
            <input value={props.searchValue} onChange={e => props.setSearchValue(e.target.value)} className="search-input" placeholder="Author search..." />
         </div>
         <div>
            <h3>How many photos to display per page?</h3>
            {/* Сколько фото отображать */}
            <input value={props.numberOfPhotos} onChange={e => props.setNumberOfPhotos(e.target.value)} className="search-input" type="number" placeholder="number..." />
         </div>
         {/* Контент */}
         <div className="content">
            {props.loading ?
               <h2>Loading...</h2> :
               props.photo.filter(obj => {
                  return obj.author.toLowerCase().includes(props.searchValue.toLowerCase())
               }).map((obj) => (
                  // Компонента с фото
                  <Photo key={obj.id}
                     id={obj.id}
                     author={obj.author}
                     url={obj.download_url}
                  />
               ))
            }

         </div>
         <ul className="pagination">
            {
               // Пагинация
               [...Array(5)].map((_, i) => (
                  <li key={i} onClick={() => props.setPage(i + 1)} className={props.page === i + 1 ? 'active' : ''}>
                     {i + 1}
                  </li>
               )
               )
            }
         </ul>
      </div >
   )
}

export default Main;